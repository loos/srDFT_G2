set xrange [-2:4]
set yrange [-0.1:0]
plot "N2_cc-pvdz.mu_of_r" u 1:3 w l title "DZ"
plot "N2_cc-pvdz.mu_of_r" u 1:3 w l title "DZ", "N2_cc-pvtz.mu_of_r" u 1:3 w l title "TZ"
plot "N2_cc-pvdz.mu_of_r" u 1:3 w l title "DZ", "N2_cc-pvtz.mu_of_r" u 1:3 w l title "TZ", "N2_cc-pvqz.mu_of_r" u 1:3 w l title "QZ"
set output "ec_of_r.eps"
set term eps
replot

