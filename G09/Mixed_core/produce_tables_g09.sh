source list_molecules
source list_atoms
basis=$1
rm data_HF_${basis}_g09
rm data_CC_${basis}_g09

for j in $list_mol
 do
 system=$j
 file=Molecules/${basis}/${j}.out
 EHF=`grep "E(ROHF) =" $file  | cut -d "=" -f 2 | cut -d "A" -f 1`
 ECCSDT=`grep "CCSD(T)=" $file  |  head -1 | cut -d "=" -f 2 | cut -d "A" -f 1`
 echo $system $EHF    >> data_HF_${basis}_g09
 echo $system $ECCSDT >> data_CC_${basis}_g09
done


for j in $list_atom
 do
 system=$j
 file=Atoms/${basis}/${j}.out
 EHF=`grep "E(ROHF) =" $file  | cut -d "=" -f 2 | cut -d "A" -f 1`
 ECCSDT=`grep "CCSD(T)=" $file  |  head -1 | cut -d "=" -f 2 | cut -d "A" -f 1`
 echo $system $EHF    >> data_HF_${basis}_g09
 echo $system $ECCSDT >> data_CC_${basis}_g09
done

