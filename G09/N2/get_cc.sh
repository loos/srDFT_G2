atom=$1
molecule=${1}2
icount=2
for i in vdz vtz vqz v5z
 do
 file=
#Eccat[icount]=`grep "CCSD(T)= " ${atom}_${i}.out | cut -d "=" -f 2`
#Ehfat[icount]=`grep "E(ROHF) =" ${atom}_${i}.out | cut -d "=" -f 2`
 tmp=`grep "CCSD(T)= " ${molecule}_${i}.out | cut -d "=" -f 2`
 tmp=${tmp/D/E}
 Eccmol[icount]=$tmp
 Ehfmol[icount]=`grep "E(ROHF) =" ${molecule}_${i}.out | cut -d "=" -f 2 | cut -d "A" -f 1`
 Ecormol[icount]=`echo print  ${Eccmol[$icount]} - ${Ehfmol[$icount]} | python`
 echo $icount  ${Ehfmol[$icount]} ${Eccmol[$icount]} ${Ecormol[$icount]} >> data_mol
 icount=$(($icount + 1))
done

denom=`echo print 5**3 - 4**3 | python`
num=`echo print ${Ecormol[5]}* 5**3  - ${Ecormol[4]}* 4**3   | python`
Ecormolextrap=`echo print $num/$denom | python`
Etotmolextrap=`echo print ${Ehfmol[5]} + $Ecormolextrap | python`
echo $Ecormolextrap $Etotmolextrap

#denom=`echo print 5**3 - 4**3 | python`
#num=`echo print ${Ecorat[5]}* 5**3  - ${Ecorat[4]}* 4**3   | python`
#Ecoratextrap=`echo print $num/$denom | python`
#echo $Eccatextrap

