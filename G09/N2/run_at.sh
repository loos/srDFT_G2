#! /bin/bash
#SBATCH -p xeonv1_mono -c 1 -n 1 -N 1

module load g09/d01

for INP in $( ls N_v*.inp ); do
  MOL=${INP%.*}
  g09 ${MOL}.inp ${MOL}.out
done

