#! /bin/bash
#SBATCH -p xeonv2 -c 20 -n 1 -N 1

module load g09/d01

for INP in $( ls *.inp ); do
  MOL=${INP%.*}
  g09 ${MOL}.inp ${MOL}.out
done

