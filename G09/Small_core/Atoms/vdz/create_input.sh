#! /bin/bash

if [ $# != 1 ]
then
  echo "Please provide template file" 
fi

if [ $# = 1 ]
then

  for XYZ in $( ls *.g09_zmat ); do
    MOL=${XYZ%.*}
    cat $1 ${MOL}.g09_zmat > ${MOL}.inp
    echo >> ${MOL}.inp
    echo >> ${MOL}.inp
  done

fi

