#!/bin/bash 

# jobname: 
#SBATCH -J D

# initial working directory: 
#SBATCH -D ./

# standard output and error: 
#SBATCH -o ./test.out.%j
#SBATCH -e ./test.err.%j

# queue or partition:
#SBATCH --partition=p.48h

# Number of nodes and cores: 
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --tasks-per-node=1 
#SBATCH --cpus-per-task=40

# mail: 
#SBATCH --mail-type=end,fail
#SBATCH --mail-user=e.giner@fkf.mpg.de

# wall clock limit: 
#SBATCH --time=47:58:00

# run the program:

module purge
source ~/.bashrc
 . ~/qp2/quantum_package.rc

#export OMP_NUM_THREADS=1

export OMP_NUM_THREADS=40 # 40 coeurs
export OMP_PROC_BIND=false

############################################################################


for i in *ezfio
 do
  ezfio=$i

  qp_run scf         $ezfio | tee ${ezfio}.scf_1.out
  qp_set_frozen_core ${ezfio}
  qp_run cis         $ezfio | tee ${ezfio}.cis.out
  qp_run save_natorb $ezfio | tee ${ezfio}.natorb.out
  qp_run scf         $ezfio | tee ${ezfio}.scf_2.out
  qp_run all_mu_of_r_corrections ${ezfio} | tee ${ezfio}.DFT.out

done

