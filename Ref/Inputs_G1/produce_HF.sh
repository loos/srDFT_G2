source list_molecules 
source list_atoms

basis=$1

rm data_HF_$basis 
#for i in $list_atom
# do 
#  file="${basis}/${i}.ezfio.scf_2.out"
#  EHF=`grep "SCF energy" $file | cut -d "y" -f 2`
#  echo $i $EHF >> data_HF_$basis
#done

for i in $list_mol 
 do 
  file="${basis}/${i}.ezfio.scf_2.out"
  EHF=`grep "SCF energy" $file | cut -d "y" -f 2`
  echo $i $EHF >> data_HF_$basis
done
