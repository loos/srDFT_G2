#! /bin/bash

if [ $# != 1 ]
then
  echo "Please provide template file" 
fi

if [ $# = 1 ]
then

  for XYZ in $( ls *.xyz ); do
    MOL=${XYZ%.*}
    cat $1 ${MOL}.xyz > ${MOL}.inp
  done

fi

