
atom=$1
mol=${atom}2
file=$2

Eatom=`grep -w "${atom}" $file  | cut -d "-" -f 2`
Emol=` grep -w "${mol}"  $file  | cut -d "-" -f 2`
Eatom=-${Eatom}
Emol=-${Emol}
de=`echo "($Eatom * 2 - $Emol) * 627.509608" | bc `
echo $de
