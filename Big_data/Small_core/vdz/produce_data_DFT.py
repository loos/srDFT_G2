#!/usr/bin/env python2

import sys

basis = sys.argv[1]

filecc = "data_CC_"+basis+"_g09"
fileLDA = "data_LDA_"+basis
filePBE = "data_PBE_"+basis

cc = []
LDA_ful = []
LDA_val = []
PBE_ful = []
PBE_val = []
system = []

with open(filecc, "r") as fp:
   for line in fp:
     a=line.split()
     system.append(a[0])
     b = a[1].replace("D","E")
     cc.append(float(b))

with open(fileLDA, "r") as fp:
   for line in fp:
     a=line.split()
     LDA_ful.append(float(a[1]))
     LDA_val.append(float(a[2]))

with open(filePBE, "r") as fp:
   for line in fp:
     a=line.split()
     PBE_ful.append(float(a[1]))
     PBE_val.append(float(a[2]))

file_output_cc      = open("data_CC_"+basis,"w+")
file_output_LDA_ful = open("data_CC+LDA_ful_"+basis,"w+")
file_output_LDA_val = open("data_CC+LDA_val_"+basis,"w+")
file_output_PBE_ful = open("data_CC+PBE_ful_"+basis,"w+")
file_output_PBE_val = open("data_CC+PBE_val_"+basis,"w+")
icount = 0
for e in cc:
 file_output_cc.write(system[icount]+'  '+str(e) +'\n')
 file_output_LDA_ful.write(system[icount]+'  '+str(e + LDA_ful[icount]) +'\n')
 file_output_LDA_val.write(system[icount]+'  '+str(e + LDA_val[icount]) +'\n')
 file_output_PBE_ful.write(system[icount]+'  '+str(e + PBE_ful[icount]) +'\n')
 file_output_PBE_val.write(system[icount]+'  '+str(e + PBE_val[icount]) +'\n')
 icount += 1
