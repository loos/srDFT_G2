#!/usr/bin/env python2

import sys

basis5 = "v5z"
basis4 = "vqz"

fileCC5 = "data_CC_"+basis5
fileCC4 = "data_CC_"+basis4

fileHF5 = "data_HF_"+basis5
fileHF4 = "data_HF_"+basis4

CC5 = []
CC4 = []

HF5 = []
HF4 = []

Ecor4 = []
Ecor5 = []

system = []

with open(fileCC4, "r") as fp:
   for line in fp:
     a=line.split()
     b = a[1].replace("D","E")
     system.append(a[0])
     CC4.append(float(b))

with open(fileCC5, "r") as fp:
   for line in fp:
     a=line.split()
     b = a[1].replace("D","E")
     system.append(a[0])
     CC5.append(float(b))

with open(fileHF4, "r") as fp:
   for line in fp:
     a=line.split()
     b = a[1].replace("D","E")
     system.append(a[0])
     HF4.append(float(b))

with open(fileHF5, "r") as fp:
   for line in fp:
     a=line.split()
     b = a[1].replace("D","E")
     system.append(a[0])
     HF5.append(float(b))

def extrapecor(EX,EY,X,Y):
    denom = (X**3 - Y**3)
    num = (EX*X**3 - EY*Y**3)
    return num/denom

file_output_extrap  = open("data_extrap_CC_Q5","w+")
icount = 0
for e in CC4:
 Ecor4.append(CC4[icount] - HF4[icount])
 Ecor5.append(CC5[icount] - HF5[icount])
 ecor = extrapecor(Ecor5[icount], Ecor4[icount], 5, 4)
 print Ecor4[icount], Ecor5[icount], ecor

 file_output_extrap.write(system[icount]+'  '+str(HF5[icount] + ecor ) +'\n')
 icount += 1
