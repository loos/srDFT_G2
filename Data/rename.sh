#!/bin/bash
  
set -e 

if [ $# != 1 ]
then
  echo "One argument required [OUTPUT] !!" 
fi

if [ $# = 1 ]
then

  OUTPUT=$1
  for i in *.dat ; do MOL=${i%_*} ; mv $i ${MOL}_${OUTPUT}.dat ; done

fi
