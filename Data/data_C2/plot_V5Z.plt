   set log x
   set format x "10^{%T}"
   set xlabel "threshold CI"
   set ylabel "D_e"
   set key font "10,15"
   set key center right
   plot "data_V5Z_pbe_val" w lp title "V5Z PBE val"
   set output "V5Z_pbe_val.eps"
   set term eps
   replot
