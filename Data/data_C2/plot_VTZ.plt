   set log x
   set format x "10^{%T}"
   set xlabel "threshold CI"
   set ylabel "D_e"
   set key font "10,15"
   set key center right
   plot "data_VTZ_pbe_val" w lp title "VTZ PBE val"
   set output "VTZ_pbe_val.eps"
   set term eps
   replot
