import sys
import os


system = []
ev = []
hf_lda = []
val_lda = []


filepath = 'G2_cc-pVTZ.dat'

with open(filepath, "r") as fp:
   cipsi = []
   for line in fp:
     a=line.split()
     cipsi.append(line.split())
     system.append(a[0])
     ev.append(a[1])


#print cipsi

filepath = 'data_HF_PBE_new_VTZ'
with open(filepath, "r") as fp2:
   for line in fp2:
     a=line.split()
 #   print a[0], a[1]
     hf_lda.append(a[1])
     val_lda.append(a[2])

file_PBE = open("G2_CIPSI_VTZ_PBE_new.dat","w+")
count=0
for e in ev:
  file_PBE.write(system[count] +'  '+str(float(e)+float(hf_lda[count]))+'\n')
  count += 1


file_PBE_val = open("G2_CIPSI_VTZ_PBE_new_valence.dat","w+")
count=0
for e in ev:
  if (count==1):
   print float(e), float(val_lda[count]) , float(hf_lda[count])

  file_PBE_val.write(system[count] +'  '+''+str(float(e)+float(val_lda[count]))+'\n')
  count += 1


